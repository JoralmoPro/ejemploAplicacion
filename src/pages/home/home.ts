import { DetalleUsuarioPage } from './../detalle-usuario/detalle-usuario';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  usuarios = [
    {
      "nombre":"Jose",
      "direccion":"Mzna 33 casa 18 el parque",
      "telefono":"3002892012"
    },
    {
      "nombre":"Hernan",
      "direccion":"Mzna B casa 11 villa monica",
      "telefono":"3016756656"
    },
    {
      "nombre":"Ruben",
      "direccion":"Mzna D casa 21 villa universitaria",
      "telefono":"3045336862"
    },
    {
      "nombre":"Alex",
      "direccion":"Carrera 1a # 44-19",
      "telefono":"3017304188"
    },
    {
      "nombre":"Enderson",
      "direccion":"Calle 26 carrera 12",
      "telefono":"3015570809"
    }
  ];

  constructor(public navCtrl: NavController) {
  }

  irDetalleUsuario(usuario){
    this.navCtrl.push(DetalleUsuarioPage, {usuario:usuario});
  }
}